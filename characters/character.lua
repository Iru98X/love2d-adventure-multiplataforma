-- Clase generica que representa un personaje, compuesto por su SpriteSheet y el numero de frames
-- y animaciones correspondientes
local Character = {
New = function()
    local character = {
                        Animations = {},
                        orden = {},

                        Hitboxes = {},
                        Walkbox = {},
                        Collisionbox = {},
                        spriteSheets,
                        numFrames,
                        numAnimations
                    }

    -- Funcion que, dado un personaje y un orden de animaciones, genera quads que representan las distintas animaciones
    function character:generateAnimations()

        local animation = {}
        local numF
        local xoffset = math.floor(self.spriteSheets[1]:getWidth()/self.numFrames)
        local yoffset = math.floor(self.spriteSheets[1]:getHeight()/self.numAnimations)
        for y = 0, self.numAnimations - 1 do
            for x = 0, self.numFrames - 1 do
                table.insert(animation, {x*xoffset, y*yoffset, xoffset, yoffset})
            end
            self:addAnimation(animation, self.orden[y+1])
            animation = {} -- resetea la tabla con la animacion
        end

        if (#self.spriteSheets > 1) then
            local animation = {}
            local numF
            local xoffset = math.floor(self.spriteSheets[2]:getWidth()/self.numFramesAtk)
            local yoffset = math.floor(self.spriteSheets[2]:getHeight()/self.numAnimationsAtk)
            for y = 0, self.numAnimationsAtk - 1 do
                for x = 0, self.numFramesAtk - 1 do
                    table.insert(animation, {x*xoffset, y*yoffset, xoffset, yoffset})
                end
                self:addAnimation(animation, self.orden[4+(y+1)])
                animation = {} -- resetea la tabla con la animacion
            end
        end
    end

    function character:addAnimation(a, id)
        self.Animations[id] = a
    end
        
    function character:addHitbox(h, id)
        self.Hitboxes[id] = h
    end
        
    function character:getAnimations()
        return self.Animations
    end

    function character:getAnimation(id)
        return self.Animations[id]
    end
        
    function character:getHitboxes()
        return self.Hitboxes
    end
    
    return character
end
}

return Character