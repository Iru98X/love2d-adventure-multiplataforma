local Knight = require("characters/character").New()

Knight.spriteSheets = {love.graphics.newImage("assets/personajes/knight.png")}
Knight.orden = {"abajo","izquierda","derecha","arriba","se","sw","ne","nw"}
Knight.Hitboxes = { {45,19,6,10},
                    {40,25,16,33},
                    {40,59,6,24},{49,59,6,24},
                    {34,26,6,31},{56,26,6,31} }
Knight.Walkbox = {38,75,19,8}
Knight.Collisionbox = {37,26,22,57}
Knight.numFrames = 8
Knight.numAnimations = 8

Knight:generateAnimations()

return Knight
