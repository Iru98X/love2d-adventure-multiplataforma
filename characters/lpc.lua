local Lpc = require("characters/character").New()

Lpc.spriteSheets = {love.graphics.newImage("assets/personajes/Lpc/walkcycle/BODY_male.png"),
                    love.graphics.newImage("assets/personajes/Lpc/slash/BODY_human.png")}
Lpc.orden = {"arriba","izquierda","abajo","derecha","meleeArr","meleeIzq","meleeAbj","meleeDch"}
Lpc.Hitboxes = { {21,12,22,20},
                    {22,33,20,19},
                    {20,53,9,8},{35,53,9,8},
                    {18,33,4,17},{42,33,4,17} }
Lpc.Walkbox = {20,54,24,7}
Lpc.Collisionbox = {22,9,19,45}

Lpc.numFrames = 9
Lpc.numAnimations = 4

Lpc.numFramesAtk = 6
Lpc.numAnimationsAtk = 4

Lpc:generateAnimations()

return Lpc