local Player = require("characters/character").New()

Player.spriteSheets = {love.graphics.newImage("assets/personajes/professor_walk.png")}
Player.orden = {"arriba","izquierda","abajo","derecha"}
Player.Hitboxes = { {21,12,22,20},
                    {22,33,20,19},
                    {20,53,9,8},{35,53,9,8},
                    {18,33,4,17},{42,33,4,17} }
Player.Walkbox = {20,54,24,7}
Player.numFrames = 9
Player.numAnimations = 4

Player:generateAnimations()

return Player


