local Skeleton = require("characters/character").New()

Skeleton.spriteSheets = {love.graphics.newImage("assets/personajes/skeleton.png")}
Skeleton.orden = {"arriba","izquierda","abajo","derecha"}
Skeleton.Hitboxes = { {22,9,20,17},
                      {22,27,19,17},
                      {20,45,8,9},{36,45,8,9},
                      {18,27,3,16},{42,27,3,16} }
Skeleton.Walkbox = {20,48,24,7}
Skeleton.Collisionbox = {22,9,19,45}
Skeleton.numFrames = 9
Skeleton.numAnimations = 4

Skeleton:generateAnimations()

return Skeleton


