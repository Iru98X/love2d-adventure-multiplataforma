local component = require "components/component"

local C_Inventory = {
New = function()
    local c_inventory = component.New "inventory"

    function c_inventory.Load(e, args)
        e.Money = 0
    end

    function c_inventory.Tick(e, dt)
        
    end

    function c_inventory.Render(e)
    end

    return c_inventory
end
}
return C_Inventory
