local rect = love.graphics.rectangle
local draw = love.graphics.draw

-- Componente que representa el sprite de una entidad
local C_Sprite = {
    New = function()
        local c_sprite = require "components/component".New("sprite")
        local parpadeo = 0

        function c_sprite.Load(e, args)

            e.type = args.type

            e.numFrameActual = 0
            e.animActual = "abajo"
            e.velAnimacion = args.velAnimacion * 30
            e.animTimer = 1/e.velAnimacion
            e.meleeTimer = 1/(e.velAnimacion*3)
            e.spriteActual = love.graphics.newQuad(0,0,math.floor(_G[e.type].spriteSheets[1]:getWidth()/_G[e.type].numFrames),
                                                math.floor(_G[e.type].spriteSheets[1]:getHeight()/_G[e.type].numAnimations),
                                                _G[e.type].spriteSheets[1]:getWidth(),
                                                _G[e.type].spriteSheets[1]:getHeight())
            e.parado = true
            e.parpadeando = false
            e.accion = 1
            e.itemsRenderIdle = {}
            e.itemsRenderAtk = {}

            if ( e.type == "Lpc") then
                local itemsIniciales = {1,2,7,11,12,15,20,22,23}
                -- Inserta los items iniciales
                for i=1, #itemsIniciales do
                    table.insert(e.itemsRenderIdle, Items[itemsIniciales[i]])
                    table.insert(e.itemsRenderAtk, Items[itemsIniciales[i]+28])
                end
                table.insert(e.itemsRenderAtk, Items[55])
            end
        end

        function c_sprite.Tick(e, dt)
            Game.Animations:Anima(e, dt)
        end

        function c_sprite.Render(e)
            -- Renderiza la entidad y parpadea si recibe dano cuando no esta en pause
            if not Game.Pause then
                if e.parpadeando and (parpadeo%30==0 or parpadeo%30==1 or parpadeo%30==2 or parpadeo%30==3 or parpadeo%30==4 or parpadeo%30==5 or parpadeo%30==6 or parpadeo%30==7) then
                    draw(love.graphics.newImage("assets/personajes/Empty.png"), e.spriteActual, e.Position.x, e.Position.y)
                    parpadeo = parpadeo + 1
                else
                    -- Render personaje
                    draw(_G[e.type].spriteSheets[e.accion], e.spriteActual, e.Position.x, e.Position.y)
                    
                    -- Render items
                    if (e.accion == 1) then
                        for _, v in ipairs (e.itemsRenderIdle) do
                            draw(v[1], e.spriteActual, e.Position.x, e.Position.y)
                        end
                    end

                    if (e.accion == 2) then
                        for _, v in ipairs (e.itemsRenderAtk) do
                            draw(v[1], e.spriteActual, e.Position.x, e.Position.y)
                        end
                    end

                    parpadeo = parpadeo + 1
                end
            else
                -- Juego en pause

                -- Render personaje
                draw(_G[e.type].spriteSheets[e.accion], e.spriteActual, e.Position.x, e.Position.y)

                -- Render items
                for _, v in ipairs (e.itemsRenderIdle) do
                    draw(v[1], e.spriteActual, e.Position.x, e.Position.y)
                end
            end
        end

        return c_sprite
    end,
}
return C_Sprite