local component = require "components/component"

local C_Stats = {
New = function()
    local c_stats = component.New "stats"
    local areaHP = {}

    function c_stats.Load(e, args)
        e.lvl = args.lvl or 1
        e.Speed = args.speed or 18
        e.Health = args.health or 100
        e.MaxHealth = e.Health
        e.Damage = args.damage or 20
        e.Inmortal = false
        e.atacando = false
        areaHP = {  x = math.floor((e.Position.x+e.Size.x/2)-math.min(e.MaxHealth/2,80)/2),
                    y = math.floor(e.Position.y-10),
                    w = math.min(e.MaxHealth/2,80),
                    h = 7,
                    sw = math.min(math.floor(e.Health/2),80) }
    end

    function c_stats.Tick(e, dt)
        areaHP = { x = math.floor((e.Position.x+e.Size.x/2)-math.min(e.MaxHealth/2,80)/2),
                     y = math.floor(e.Position.y-10),
                     w = math.min(e.MaxHealth/2,80),
                     h = 7,
                     sw = math.min(math.floor(e.Health/2),80)
                    }
        if e.Health <= 0 then
            if e.type == "Lpc" or e.type == "Player" then
                revive(e)
            elseif e.type == "Skeleton" or e.type == "Knight" then
                e.Remove = true
                e.value = 1
                Game.Physics:Add_Drop(e)
            end
        end
    end

    function c_stats.Render(e)
        -- Dibuja HP a los NPCs
        if not (e.type == "Player" or e.type == "Lpc") then
            local r,g,b,c = love.graphics.getColor()
            love.graphics.setColor(0, 0, 0, 180)
            love.graphics.rectangle( 'fill', areaHP.x-1, areaHP.y-1, areaHP.w+2, areaHP.h+2, 3, 3)
            love.graphics.setColor(255, 0, 0, 180)
            love.graphics.rectangle( 'fill', areaHP.x, areaHP.y, areaHP.sw, areaHP.h, 3, 3)
            love.graphics.setColor(r,g,b,c)
        end
    end


    function revive(e)
        e.Health = e.MaxHealth
        e.Position = {x=e.Spawn.x, y=e.Spawn.y}
        Game.UI:ActualizaHP(e.MaxHealth, e.MaxHealth)
    end

    return c_stats
end
}
return C_Stats