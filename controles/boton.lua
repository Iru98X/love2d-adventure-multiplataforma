require("tools/vect2")
local vect2 = math.vect2

local Boton = {
New = function(sprite, x, y, scale, clickable, activo, id)
	local boton = {
		sprite,
		pos,
		scale,
		clickable,
		activo,
		id
	}

	function boton:Init()
		self.sprite 	= love.graphics.newImage(sprite)
		self.pos 		= vect2.New(x, y)
		self.scale 		= scale
		self.clickable 	= clickable
		self.activo 	= activo
		self.id 		= id
		self.posScale 	= vect2.New(self.pos.x/self.scale,
									self.pos.y/self.scale)
		Game.UI:AddButton(self)
	end

	function boton:Render()
		if self.activo == true then
			love.graphics.push()
			love.graphics.scale(self.scale,self.scale)
			love.graphics.draw(self.sprite, self.posScale.x, self.posScale.y)
			love.graphics.pop()
			if Debug_Mode then
				love.graphics.rectangle("line", boton.pos.x,
												boton.pos.y,
												boton.sprite:getWidth()*self.scale,
												boton.sprite:getHeight()*self.scale )
			end
		end
	end

	function boton:Destroy()
		
	end

	function boton:mousepressed(x,y)
	
	end

	function boton:touchpressed(x,y)
	
	end

	function boton:estaEnZona(x,y)
		if self.activo == true then
			if self.clickable == true then
				if x >= self.pos.x and
					x <= self.pos.x + self.sprite:getWidth()*self.scale and
					y >= self.pos.y and
					y <= self.pos.y + self.sprite:getHeight()*self.scale then
					return true
				end
			end
		else
			return false
		end
	end

	boton:Init()
	if boton.clickable then
		Game.Mouse:addButton(boton)
		Game.Touch:addButton(boton)
	end

	return boton
end,
}
return Boton