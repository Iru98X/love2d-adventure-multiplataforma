require("tools/vect2")
local vect2 = math.vect2

local BotonCentrado = {
New = function(sprite, x, y, scale, clickable, activo, id)
	local botonCentrado = {
		sprite,
		pos,
		scale,
		clickable,
		activo,
		id
	}

	function botonCentrado:Init()
		self.sprite 	= love.graphics.newImage(sprite)
		self.pos 		= vect2.New(x, y)
		self.scale 		= scale
		self.clickable 	= clickable
		self.activo 	= activo
		self.id			= id
		self.posScale 	= vect2.New((self.pos.x-(self.sprite:getWidth()/2)*self.scale)/self.scale,
								    (self.pos.y-(self.sprite:getHeight()/2)*self.scale)/self.scale)
		Game.UI:AddButton(self)
	end

	function botonCentrado:Render()
		if self.activo==true then
			love.graphics.push()
			love.graphics.scale(self.scale,self.scale)
			love.graphics.draw(self.sprite, self.posScale.x, self.posScale.y)
			love.graphics.pop()
			if Debug_Mode then
				love.graphics.rectangle("line", botonCentrado.pos.x-(botonCentrado.sprite:getWidth()/2)*self.scale,
												botonCentrado.pos.y-(botonCentrado.sprite:getHeight()/2)*self.scale,
												botonCentrado.sprite:getWidth()*self.scale,
												botonCentrado.sprite:getHeight()*self.scale )
			end
		end
	end

	function botonCentrado:estaEnZona(x,y)
		if self.activo == true then
			if self.clickable == true then
				if x >= botonCentrado.pos.x-(botonCentrado.sprite:getWidth()/2)*botonCentrado.scale and
					x <= botonCentrado.pos.x-(botonCentrado.sprite:getWidth()/2)*botonCentrado.scale + botonCentrado.sprite:getWidth()*botonCentrado.scale and
					y >= botonCentrado.pos.y-(botonCentrado.sprite:getHeight()/2)*botonCentrado.scale and
					y <= botonCentrado.pos.y-(botonCentrado.sprite:getHeight()/2)*botonCentrado.scale + botonCentrado.sprite:getHeight()*botonCentrado.scale then
					return true
				end
			end
		else
			return false
		end
	end

	botonCentrado:Init()
	if botonCentrado.clickable then
		Game.Mouse:addButton(botonCentrado)
		Game.Touch:addButton(botonCentrado)
	end

	return botonCentrado
end,
}
return BotonCentrado