local BotonNoClickable = {
    New = function(sprite, x, y, scale, activo, id)
    
        local botonNoClickable = require("controles/botonCentrado").New(sprite, x, y, scale, false, activo, id)
    
        return botonNoClickable
    end,
}
return BotonNoClickable