local Boton = {
New = function(sprite, x, y, scale, clickable, activo, id)
    
    local boton = require("controles/botonCentrado").New(sprite, x, y, scale, clickable, activo, id)

    function boton:mousepressed(x,y)
        if self:estaEnZona(x,y) then
            if(Game.Pause == false) then
            self:funcion()
            end
        end
    end

    function boton:touchpressed( id, x, y, dx, dy, pressure )
        if self:estaEnZona(x,y) then
            self:funcion()
        end
    end

    function boton:funcion()
        e.atacando = true
        e.accion = 2
        e.numFrameActual = 1
        Game.Sounds:playRandSound(Game.Sounds.sword)
        Game.Physics:resetTimerAtacando()
        local target = Game.Physics:selectClosestNPC(e)
        if not (target.type == "nul") then
            target.Health = target.Health - e.Damage
        end
    end

return boton
end,
}
return Boton