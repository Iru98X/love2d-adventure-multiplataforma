local BotonDebug = {
    New = function(sprite, x, y, scale, clickable, activo, id)
    
        local botonDebug = require("controles/boton").New(sprite, x, y, scale, clickable, activo, id)
    
        function botonDebug:mousepressed(x,y)
            if self:estaEnZona(x,y) then
                if Debug_Mode then
                    Debug_Mode = false
                else
                    Debug_Mode = true
                end
            end
        end
    
        function botonDebug:touchpressed( id, x, y, dx, dy, pressure )
            if self:estaEnZona(x,y) then
                if Debug_Mode then
                    Debug_Mode = false
                else
                    Debug_Mode = true
                end
            end
        end
    
        return botonDebug
    end,
}
return BotonDebug