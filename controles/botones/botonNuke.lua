local Boton = {
New = function(sprite, x, y, scale, clickable, activo, id)
    
    local boton = require("controles/botonCentrado").New(sprite, x, y, scale, clickable, activo, id)

    function boton:mousepressed(x,y)
        if self:estaEnZona(x,y) then

            -- Sonido
            Game.Sounds:playSound(Game.Sounds.explosion)

            Game.KillAll = true
        end
    end

    

    function boton:touchpressed( id, x, y, dx, dy, pressure )
        if self:estaEnZona(x,y) then
            
            -- Sonido
            Game.Sounds:playSound(Game.Sounds.explosion)

            Game.KillAll = true
        end
    end

return boton
end,
}
return Boton