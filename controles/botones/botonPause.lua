local BotonPause = {
    New = function(sprite, x, y, scale, clickable, activo, id)
    
        local botonPause = require("controles/boton").New(sprite, x, y, scale, clickable, activo, id)
    
        function botonPause:mousepressed(x,y)
            if self:estaEnZona(x,y) then
                if Game.Pause == false then
                    Game.Pause = true
                else
                    Game.Pause = false
                end
            end
        end
    
        function botonPause:touchpressed( id, x, y, dx, dy, pressure )
            if self:estaEnZona(x,y) then
                if Game.Pause == false then
                    Game.Pause = true
                    Game.Joystick.activo = false
                else
                    Game.Pause = false
                    Game.Joystick.activo = true
                end
            end
        end
    
        return botonPause
    end,
}
return BotonPause