local Boton = {
New = function(sprite, x, y, scale, clickable, activo, id)
    
    local boton = require("controles/botonCentrado").New(sprite, x, y, scale, clickable, activo, id)

    function boton:mousepressed(x,y)
        if self:estaEnZona(x,y) then  
            
            self.activo = false

            -- Sonido
            Game.Sounds:playSound(Game.Sounds.boton)

            -- Cargar escena 1
            Game.Assets:Add(love.graphics.newImage("assets/maps/tileset1.png"), "tileset1")
            Game.Assets:Generate_Quads(32, Game.Assets:Get("tileset1"), "tileset1_quads")
            Game.Mouse:removeButton(self)
            Game.UI:RemoveButton(self)
            Game.GSM:GotoScene(require "scenes/charging")
            self=nil
        end
    end

    

    function boton:touchpressed( id, x, y, dx, dy, pressure )
        if self:estaEnZona(x,y) then

            self.activo = false

            -- Sonido
            Game.Sounds:playSound(Game.Sounds.boton)
            
            -- Cargar escena 1
            Game.Assets:Add(love.graphics.newImage("assets/maps/tileset1.png"), "tileset1")
            Game.Assets:Generate_Quads(32, Game.Assets:Get("tileset1"), "tileset1_quads")
            Game.GSM:GotoScene(require "scenes/charging")
            Game.Mouse:removeButton(self)
            self=nil
        end
    end

return boton
end,
}
return Boton