require("controles/boton")

local Mouse = {}

function Mouse:Init()
    self.botones = {}
end

function Mouse:addButton(bot)
    table.insert(self.botones, bot)
end

function Mouse:removeButton(bot)
    for k,v in pairs(self.botones) do
        if v == bot then
            table.remove(self.botones, k)
            break
        end
    end
end

function love.mousepressed(x, y)
    if (Game.OS == "Linux" or Game.OS == "Windows") then
        for i, boton in ipairs(Mouse.botones) do
            boton:mousepressed(x, y)
        end
    end
end

return Mouse