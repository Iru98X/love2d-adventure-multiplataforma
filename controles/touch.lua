require("controles/boton")
require("controles/joystick")

local Touch = {}

function Touch:Init()
    self.botones = {}
    self.joystick = {}
end

function Touch:addButton(bot)
    table.insert(self.botones, bot)
end

function Touch:addJoystick(js)
    table.insert(self.joystick, js)
end

function Touch:removeJoystick(bot)
    for k,v in pairs(self.joystick) do
        if v == bot then
            table.remove(self.joystick, k)
            break
        end
    end
end

function love.touchpressed( id, x, y, dx, dy, pressure )
    if (Game.OS == "Android") then
        for i, j in ipairs(Touch.joystick) do
            if j~= nil then
                j:touchpressed(id, x, y, dx, dy, pressure)
                if Debug_Mode then
                    debugCoords(x,y)
                end
            end
        end
        for i, boton in ipairs(Touch.botones) do
            boton:touchpressed( id, x, y, dx, dy, pressure )
        end
        
    end
end

function love.touchmoved( id, x, y, dx, dy, pressure )
    for i, j in ipairs(Touch.joystick) do
        if j~= nil then
            j:touchmoved(id, x, y, dx, dy, pressure)
            if Debug_Mode then
                debugCoords(x,y)
            end
        end
    end
end

function love.touchreleased( id, x, y, dx, dy, pressure )
    for i, j in ipairs(Touch.joystick) do
        if j~= nil then
            j:touchreleased(id, x, y, dx, dy, pressure)
        end
    end
end

function debugCoords(x,y)
    love.graphics.print(tostring(x)..", "..tostring(y),10,50)
end

return Touch