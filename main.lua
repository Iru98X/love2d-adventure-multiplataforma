--
--   
-- _______________________|      |____________________________________________
--          ,--.    ,--.          ,--.   ,--.
--         |oo  | _  \  `.       | oo | |  oo|
-- o  o  o |~~  |(_) /   ;       | ~~ | |  ~~|o  o  o  o  o  o  o  o  o  o  o
--         |/\/\|   '._,'        |/\/\| |/\/\|
--________________________        ____________________________________________
--                        |      |

--                                                    /\
--                                                   /  \
--                                                  /    \
--                                                  | vv |
--                                                  | vv |
--                                                  | !! | 
--                                                  | !! | 
--                                                  | !! | 
--                                                  | !! | 
--                                                  | !! | 
--                                                  | !! | 
--                                                  | !! | 
--                                                  | !! | 
--           ________                               | !! |   
--          /  _____/______   ____   ____   ____    | !! |     
--         /   \  __\_  __ \_/ __ \_/ __ \ /    \   | !! |      
--         \    \_\  \  | \/\  ___/\  ___/ |  |  \  | !! |     
--          \______  /__|    \____> \____> ___|  /  | !! |     
--                                                  | !! |     
--                                                  | !! |    ________                        
--                                                  | !! |   /  _____/_____    _____   ____  
--                                                  | !! |  /   \  ___\__  \  /     \_/ __ \ 
--                                                  | !! |  \    \_\  \/ __ \|  Y Y  \  ___/ 
--                                                  | !! |   \_______ (_____ /__|_|  /\____>
--                                                  | !! |              
--                                                  | !! |
--                                                  | ^^ |
--                                     |\          /  __  \           /|
--                                     |'\________/  /  \  \_________/'|
--                                     |    []      | /\ |      []     |
--                                     |. ________  | \/ |  _________ .|
--                                     \_/        \  \__/  /         \_/
--                                                 \      /
--                                                  |XXXX| 
--                                                  |XXXX|  
--                                                  |XXXX|  
--                                                  |XXXX|   
--                                                  |XXXX|   
--                                                  |XXXX|  
--                                                  |XXXX|  
--                                                  |XXXX|  
--                                                 /  /\  \
--                                                /   --   \
--                                                \________/ 



_G.Game = {
  --  love.window.setFullscreen(true),
    GameLoop    = require "modules/gameloop",
    Renderer    = require "modules/renderer",
    World       = require "modules/entity_world",
    GSM         = require "modules/game_scene_manager",
    Assets      = require "modules/assets",
    Physics     = require "modules/physics_engine",
    Camera      = require "modules/camera",
    Animations  = require "modules/animations",
    UI          = require "modules/UI",
    Sounds      = require "modules/sounds",
    Canvas      = love.graphics.newCanvas(w,h),
    mapActual,
    Pause = false
}

--------------------------------------
--- LOVE.LOAD ------------------------
--------------------------------------
function love.load()

    local socket = require('socket')
    udp = socket.udp()
    udp:setsockname('*', 12345)
    udp:settimeout(0)

    Debug_Mode = false

    -- Inicializa las variables del juego
    Game.Dim        = { w = love.graphics.getWidth(),
                        h = love.graphics.getHeight()
                      }
    
    -- OS
    Game.OS         = love.system.getOS()
    if (Game.OS == "Linux" or Game.OS == "Windows") then
        Game.Scale      = 1.4
        Game.Rate = Game.Scale
    end
    if (Game.OS == "Android") then
        Game.Scale      = Game.Dim.w/600
        Game.Rate = Game.Scale/2
    end

    

    -- FPS
    Game.FPS = 30

    -- Fonts
    if Game.Scale == 1.4 then
        Game.Fonts = {
                        t8 = love.graphics.newFont("fonts/A Goblin Appears!.otf", 8),
                        t9 = love.graphics.newFont("fonts/A Goblin Appears!.otf", 9),
                        t10 = love.graphics.newFont("fonts/A Goblin Appears!.otf", 10),
                        t11 = love.graphics.newFont("fonts/A Goblin Appears!.otf", 11) }
    else
        Game.Fonts = {
            t8 = love.graphics.newFont("fonts/A Goblin Appears!.otf", 17),
            t9 = love.graphics.newFont("fonts/A Goblin Appears!.otf", 19),
            t10 = love.graphics.newFont("fonts/A Goblin Appears!.otf", 21),
            t11 = love.graphics.newFont("fonts/A Goblin Appears!.otf", 22),
            t12 = love.graphics.newFont("fonts/A Goblin Appears!.otf", 23) }
    end

    Game.Font = Game.Fonts.t9
    love.graphics.setFont(Game.Font)

    Game.Fondo       = love.graphics.newImage("assets/fondos/black-background.jpg")


    -- Inicializa los sistemas del juego
    Game.GameLoop:Init()
    Game.Renderer:Init(7)
    Game.Touch = require "controles/touch"
    Game.Mouse = require "controles/mouse"
    Game.Touch:Init()
    Game.Mouse:Init()
    Game.Sounds:Init()
    Game.World:Init()
    Game.Assets:Init()
    Game.GSM:Init()
    Game.Physics:Init()
    Game.Canvas:setFilter("nearest","nearest") -- Canvas

    Game.KillAll = false

    -- Carga pantalla principal
    Game.GSM:GotoScene(require "scenes/pantallaPrincipal")
    
end


---------------------------------------
-- LOVE.UPDATE ------------------------
---------------------------------------
function love.update(dt)
    if Game.Pause == false then
        Game.GameLoop:Tick(dt)
        Game.World:Tick(dt)
        Game.Camera:Update(dt)
    end
end


---------------------------------------
--- LOVE.DRAW -------------------------
---------------------------------------
function love.draw()

    -- Dibuja en el Canvas
    love.graphics.setCanvas(Game.Canvas)    -- set canvas
    love.graphics.draw(Game.Fondo, 0, 0)
    Game.Camera:Set()                       -- set camara
    Game.Renderer:Render()                  -- renderiza en el canvas
    Game.Physics:Debug_Render()
    Game.Camera:Unset()                     -- unset camara
    if Game.Joystick ~= nil and Game.OS == "Android" and Game.Joystick.activo == true then Game.Joystick:Render() end -- dibuja el js
    Game.UI:Render()
    love.graphics.setCanvas()               -- unset canvas
    love.graphics.draw(Game.Canvas, 0, 0)   -- Dibuja el Canvas
end


function love.focus(focus)
    if not focus then
        Game.Pause = true
    end
end







---------------------------------------
--- Funciones Aux ---------------------
---------------------------------------
function toScale(x)
    local x2 = x/Game.Scale
    return x2
end

function toCoords(x)
    local x2 = x*Game.Scale
    return x2
end
