local push, pop = table.insert, table.remove

local UI = {}
local texts = {}


function UI:AddButton(btn)
    push(UI, btn)
end

function UI:AddText(text, coords, id)
    push(texts, {txt = text, coords = coords, id = id})
end

function UI:RemoveButton(btn)
    for i,v in ipairs(UI) do
        if v==btn then
            pop(UI, i)
        end
    end
end

function UI:RemoveText(id)
    for i,v in ipairs(texts) do
        if v.id==id then
            pop(texts, i)
        end
    end
end

function UI:Render()
    love.graphics.print(Game.Scale, 1, 1)
    for i,v in ipairs(UI) do
        v:Render()
    end
    for i, text in ipairs(texts) do
        local r,g,b,_ = love.graphics.getColor()
        if text.id == "vida" then
            love.graphics.setColor(0, 0, 0)
        elseif text.id == "money" then
            love.graphics.setColor(0, 0, 0)
            love.graphics.setFont(Game.Fonts.t11)
        end
        love.graphics.print(text.txt, text.coords.x, text.coords.y)
        love.graphics.setColor(r,g,b)
        love.graphics.setFont(Game.Fonts.t9)
    end

    if Debug_Mode then
        --love.graphics.print("Nº bot: "..#UI,15,60*Game.Scale)
    end
end

function UI:ActualizaHP(maxhp, hp)
    for i,v in ipairs(UI) do
        if v.id == "hp" then
            local nhp = math.floor((hp*42)/maxhp)
            if nhp >= 0 and nhp <= 42 then
                UI:RemoveText("vida")
                UI:AddText(tostring(hp).." / "..tostring(maxhp),{x=math.floor(Game.Dim.w/2)-11*Game.Scale, y = math.floor((18/20)*Game.Dim.h)-2*Game.Scale},"vida")
                UI:RemoveButton(v)
                local nuevoHP = require("controles/botonNoClickable").New("assets/ui/hp/hp"..nhp..".png",math.floor(Game.Dim.w/2),math.floor((18/20)*Game.Dim.h),Game.Scale/3,true,"hp")
                break
            end
        end
    end 
end

function UI:ActualizaMoney(money)
    for i,v in ipairs(texts) do
        if v.id == "money" then
            UI:RemoveText("money")
            Game.UI:AddText(e.Money,{x=math.floor((18/20)*Game.Dim.w) + 30*Game.Scale, y = math.floor((1/5)*Game.Dim.h)},"money")
            break
        end
    end 
end

return UI