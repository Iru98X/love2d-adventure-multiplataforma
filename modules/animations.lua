local abs = math.abs

local Animations = {}

-- Importa las clases de los personajes
Items       = require("modules/items")
Player      = require("characters/player")
Skeleton    = require("characters/skeleton")
Keko        = require("characters/keko")
Robot       = require("characters/robot")
Knight      = require("characters/knight")
Lpc         = require("characters/lpc")

-- for string conversion, if you can't just do e.g. dir.toString():
local headings = { "E", "SE", "S", "SW", "W", "NW", "N", "NE" };


-- Selecciona la animacion actual de la entidad en funcion de su velocidad
function Animations:selectAnimation(e)

    -- Calcula el octante
    local angle = math.atan2( e.Velocity.y, e.Velocity.x )
    local octant = round( 8 * angle / (2*math.pi) + 8 ) % 8
    local dirStr = headings[octant+1]
    
    local animActual

    if not e.atacando then
        -- Si esta parado
        if(math.abs(e.Velocity.x) < 1 and math.abs(e.Velocity.y) < 1) then
            e.parado = true
        -- Si esta en movimiento
        else
            -- Si es una entidad con 8 direcciones
            if e.type == "Knight" then
                if     dirStr == "E" then
                    animActual = "derecha"
                elseif dirStr == "W" then
                    animActual = "izquierda"
                elseif dirStr == "N" then
                    animActual = "arriba"
                elseif dirStr == "S" then
                    animActual = "abajo"
                elseif dirStr == "NW" then
                    animActual = "nw"
                elseif dirStr == "NE" then
                    animActual = "ne"
                elseif dirStr == "SW" then
                    animActual = "sw"
                elseif dirStr == "SE" then
                    animActual = "se"
                end
            else
                -- Si es una entidad con 4 direcciones
                if(abs(e.Velocity.x) > abs(e.Velocity.y) )then
                    if(e.Velocity.x > 0) then
                        animActual = "derecha"
                    else
                        animActual = "izquierda"
                    end
                    e.parado = false
                else
                    if(e.Velocity.y > 0) then
                        animActual = "abajo"
                    else
                        animActual = "arriba"
                    end
                    e.parado = false
                end

            end
            -- Establece parado a false
            e.parado = false
        end
    else
        -- Si esta atacando
        if (e.animActual == "derecha") then
            e.animActual = "meleeDch"
            e.spriteActual = love.graphics.newQuad(0,math.floor(_G[e.type].spriteSheets[2]:getHeight()/_G[e.type].numAnimationsAtk)*3,
                                                    math.floor(_G[e.type].spriteSheets[2]:getWidth()/_G[e.type].numFramesAtk),
                                                    math.floor(_G[e.type].spriteSheets[2]:getHeight()/_G[e.type].numAnimationsAtk),
                                                    _G[e.type].spriteSheets[2]:getWidth(),
                                                    _G[e.type].spriteSheets[2]:getHeight())
        elseif (e.animActual == "izquierda") then
            e.animActual = "meleeIzq"
            e.spriteActual = love.graphics.newQuad(0,math.floor(_G[e.type].spriteSheets[2]:getHeight()/_G[e.type].numAnimationsAtk),
                                                    math.floor(_G[e.type].spriteSheets[2]:getWidth()/_G[e.type].numFramesAtk),
                                                    math.floor(_G[e.type].spriteSheets[2]:getHeight()/_G[e.type].numAnimationsAtk),
                                                    _G[e.type].spriteSheets[2]:getWidth(),
                                                    _G[e.type].spriteSheets[2]:getHeight())
        elseif (e.animActual == "abajo") then
            e.animActual = "meleeAbj"
            e.spriteActual = love.graphics.newQuad(0,math.floor(_G[e.type].spriteSheets[2]:getHeight()/_G[e.type].numAnimationsAtk)*2,
                                                    math.floor(_G[e.type].spriteSheets[2]:getWidth()/_G[e.type].numFramesAtk),
                                                    math.floor(_G[e.type].spriteSheets[2]:getHeight()/_G[e.type].numAnimationsAtk),
                                                    _G[e.type].spriteSheets[2]:getWidth(),
                                                    _G[e.type].spriteSheets[2]:getHeight())
        elseif (e.animActual == "arriba") then
            e.animActual = "meleeArr"
            e.spriteActual = love.graphics.newQuad(0,0,math.floor(_G[e.type].spriteSheets[2]:getWidth()/_G[e.type].numFramesAtk),
                                                    math.floor(_G[e.type].spriteSheets[2]:getHeight()/_G[e.type].numAnimationsAtk),
                                                    _G[e.type].spriteSheets[2]:getWidth(),
                                                    _G[e.type].spriteSheets[2]:getHeight())
        end
    end

    return animActual or e.animActual

end


function Animations:Anima(e, dt)
    -- Selecciona la animacion actual segun la velocidad de su cuerpo
    e.animActual = self:selectAnimation(e)

    -- Si esta parado, el frame actual sera el primero de cada direccion (posicion base)
    if e.accion == 1 then
        if e.parado == true then
            if e.accion == 1 then
                local primerFrame = _G[e.type]:getAnimation(e.animActual)[1]
                e.spriteActual:setViewport(primerFrame[1],primerFrame[2],primerFrame[3],primerFrame[4])
            end
        else
            -- Si no lo esta, anima el personaje
            e.animTimer = e.animTimer - dt
            -- Si el timer de cada frame llega a 0,
            if(e.animTimer <= 0) then
                
                e.animTimer = 1/e.velAnimacion -- Resetea timer

                -- Comprueba que no se pase de los frames
                e.numFrameActual = e.numFrameActual + 1

                if e.numFrameActual >= _G[e.type].numFrames then e.numFrameActual = 1 end

                -- Cambia frame
                local actualFrame = _G[e.type]:getAnimation(e.animActual)[e.numFrameActual]
                e.spriteActual:setViewport(actualFrame[1],actualFrame[2],actualFrame[3],actualFrame[4])
            end
        end
    elseif e.accion == 2 then
        e.meleeTimer = e.meleeTimer - dt
        -- Si el timer de cada frame llega a 0,
        if(e.meleeTimer <= 0) then
            e.meleeTimer = 1/(e.velAnimacion*3) -- Resetea timer
            -- Comprueba que no se pase de los frames
            e.numFrameActual = e.numFrameActual + 1
            if e.numFrameActual < _G[e.type].numFramesAtk then 
                -- Cambia frame
                local actualFrame = _G[e.type]:getAnimation(e.animActual)[e.numFrameActual]
                e.spriteActual:setViewport(actualFrame[1],actualFrame[2],actualFrame[3],actualFrame[4])
            end
        end
    end
end

function round(n)
    return n % 1 >= 0.5 and math.ceil(n) or math.floor(n)
end    

return Animations
