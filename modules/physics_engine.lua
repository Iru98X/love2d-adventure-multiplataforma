local Physics_Engine = {
    coin = love.graphics.newImage("assets/misc/coin.png")
}

local deg   = math.deg
local atan2 = math.atan2
local cos = math.cos
local sin = math.sin
local tiempoInmortal = 1.5
local tiempoAtacando = 0.3
local ratioMoneda = 0
local target = {}


function Physics_Engine:Init()
    self.bodies = {}
    self.bodies_npc = {}
    self.statics = {}
    self.map_change = {}
    self.drops = {}
    self.ratioMoneda = (Game.Scale/math.pow(Game.Rate,1.3))/10
    Game.GameLoop:AddLoop(self)
    Game.Renderer:AddRenderer(self,5)
end


function Physics_Engine:Reset()
    self.bodies = {}
    self.bodies_npc = {}
    self.statics = {}
    self.map_change = {}
    self.drops = {}
    if e ~= nil then
        e:Add(require "components/c_physics".New(), {friction = .83})
    end
end

function Physics_Engine:Create_Rect(pos, size)
    return {
        pos = pos,
        size = size,

        IntersectsBody = function(self, other)
            --for i, hitboxself in ipairs(self.Hitboxes)
            --    for j, hitboxother in ipairs(other.Hitboxes)
            --print(self.pos.x,self.pos.y,self.size.x,self.size.y)
            --print(other.pos.x,other.pos.y,other.size.x,other.size.y)

            return  self.pos.x + self.size.x > other.pos.x  and
                    self.pos.x < other.pos.x + other.size.x and
                    self.pos.y + self.size.y > other.pos.y  and
                    self.pos.y < other.pos.y + other.size.y
                    
        end
    }
end

function Physics_Engine:Add_Static_Body(rect)
    table.insert(self.statics, {
        physics_type = "static",
        Position = rect.position,
        Size     = rect.size
    })
end

function Physics_Engine:Add_Map_Change(rect)
    table.insert(self.map_change, {
        physics_type = "map_change",
        Position = rect.position,
        Size     = rect.size,
        Map= rect.map,        
        SpawnX = rect.spawnX,
        SpawnY = rect.spawnY
    })
end


function Physics_Engine:Add_Body(entity, settings)
    entity.Velocity = {x = settings.vel_x or 0, y = settings.vel_y or 0}
    entity.Friction = settings.friction or 0.95
    entity.Direction= 0

    entity.physics_type = settings.type or "dynamic"

    function entity:Get_Direction()
        deg( atan2(entity.Position.y - entity.Velocity.y, entity.Position.x - entity.Velocity.x))
    end

    function entity:Apply_Velocity(speed, direction)
        self.Velocity.x = self.Velocity.x + cos(direction) * speed
        self.Velocity.y = self.Velocity.y + sin(direction) * speed
    end

    function entity:Get_X_Body(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x + self.Velocity.x * dt, y = self.Position.y},
            self.Size
        )
    end

    function entity:Get_Y_Body(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x, y = self.Position.y + self.Velocity.y * dt},
            self.Size
        )
    end

    function entity:Get_X_BodyWalkbox(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x + _G[self.type].Walkbox[1] + self.Velocity.x * dt, y = self.Position.y + _G[self.type].Walkbox[2]},
            {x = _G[self.type].Walkbox[3], y = _G[self.type].Walkbox[4]}
        )
    end

    function entity:Get_Y_BodyWalkbox(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x + _G[self.type].Walkbox[1], y = self.Position.y + _G[self.type].Walkbox[2] + self.Velocity.y * dt},
            {x = _G[self.type].Walkbox[3], y = _G[self.type].Walkbox[4]}
        )
    end

    function entity:Get_X_BodyCollisionbox(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x + _G[self.type].Collisionbox[1] + self.Velocity.x * dt, y = self.Position.y + _G[self.type].Collisionbox[2]},
            {x = _G[self.type].Collisionbox[3], y = _G[self.type].Collisionbox[4]}
        )
    end

    function entity:Get_Y_BodyCollisionbox(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x + _G[self.type].Collisionbox[1], y = self.Position.y + _G[self.type].Collisionbox[2] + self.Velocity.y * dt},
            {x = _G[self.type].Collisionbox[3], y = _G[self.type].Collisionbox[4]}
        )
    end

    function entity:Get_X_BodyHitboxes(dt)
        local hitboxes = {}
        for i, hitbox in ipairs(_G[self.type].Hitboxes) do
            table.insert(hitboxes, Game.Physics:Create_Rect(
                                    {x = self.Position.x + hitbox[1] + self.Velocity.x * dt, y = self.Position.y + hitbox[2]},
                                    {x = hitbox[3], y = hitbox[4]} ))
        end
        return hitboxes
    end

    function entity:Get_Y_BodyHitboxes(dt)
        local hitboxes = {}
        for i, hitbox in ipairs(_G[self.type].Hitboxes) do
            table.insert(hitboxes, Game.Physics:Create_Rect(
                                    {x = self.Position.x + hitbox[1], y = self.Position.y + hitbox[2] + self.Velocity.x * dt},
                                    {x = hitbox[3], y = hitbox[4]} ))
        end
        return hitboxes
    end

    function entity:Decel()
        self.Velocity.x = self.Velocity.x * self.Friction
        self.Velocity.y = self.Velocity.y * self.Friction
    end

    function entity:Collision(other) end
    
    table.insert(self.bodies, entity)
end

function Physics_Engine:Add_Body_npc(entity, settings)
    entity.Velocity = {x = settings.vel_x or 0, y = settings.vel_y or 0}
    entity.Friction = settings.friction or 0.95
    entity.Direction= 0

    entity.physics_type = settings.type or "dynamic"

    function entity:Get_Direction()
        deg( atan2(entity.Position.y - entity.Velocity.y, entity.Position.x - entity.Velocity.x))
    end

    function entity:Apply_Velocity(speed, direction)
        self.Velocity.x = self.Velocity.x + cos(direction) * speed
        self.Velocity.y = self.Velocity.y + sin(direction) * speed
    end

    function entity:Get_X_Body(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x + self.Velocity.x * dt, y = self.Position.y},
            self.Size
        )
    end

    function entity:Get_Y_Body(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x, y = self.Position.y + self.Velocity.y * dt},
            self.Size
        )
    end

    function entity:Get_X_BodyWalkbox(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x + _G[self.type].Walkbox[1] + self.Velocity.x * dt, y = self.Position.y + _G[self.type].Walkbox[2]},
            {x = _G[self.type].Walkbox[3], y = _G[self.type].Walkbox[4]}
        )
    end

    function entity:Get_Y_BodyWalkbox(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x + _G[self.type].Walkbox[1], y = self.Position.y + _G[self.type].Walkbox[2] + self.Velocity.y * dt},
            {x = _G[self.type].Walkbox[3], y = _G[self.type].Walkbox[4]}
        )
    end

    function entity:Get_X_BodyCollisionbox(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x + _G[self.type].Collisionbox[1] + self.Velocity.x * dt, y = self.Position.y + _G[self.type].Collisionbox[2]},
            {x = _G[self.type].Collisionbox[3], y = _G[self.type].Collisionbox[4]}
        )
    end

    function entity:Get_Y_BodyCollisionbox(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x + _G[self.type].Collisionbox[1], y = self.Position.y + _G[self.type].Collisionbox[2] + self.Velocity.y * dt},
            {x = _G[self.type].Collisionbox[3], y = _G[self.type].Collisionbox[4]}
        )
    end

    function entity:Get_X_BodyHitboxes(dt)
        local hitboxes = {}
        for i, hitbox in ipairs(_G[self.type].Hitboxes) do
            table.insert(hitboxes, Game.Physics:Create_Rect(
                                    {x = self.Position.x + hitbox[1] + self.Velocity.x * dt, y = self.Position.y + hitbox[2]},
                                    {x = hitbox[3], y = hitbox[4]} ))
        end
        return hitboxes
    end

    function entity:Get_Y_BodyHitboxes(dt)
        local hitboxes = {}
        for i, hitbox in ipairs(_G[self.type].Hitboxes) do
            table.insert(hitboxes, Game.Physics:Create_Rect(
                                    {x = self.Position.x + hitbox[1], y = self.Position.y + hitbox[2] + self.Velocity.x * dt},
                                    {x = hitbox[3], y = hitbox[4]} ))
        end
        return hitboxes
    end

    function entity:Decel()
        self.Velocity.x = self.Velocity.x * self.Friction
        self.Velocity.y = self.Velocity.y * self.Friction
    end

    function entity:Collision(other) end
    
    table.insert(self.bodies_npc, entity)
end


function Physics_Engine:Add_Drop(entity)

    function entity:Get_X_Body(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x+self.Size.x/2, y = self.Position.y+self.Size.y/2},
            {x = self.coin:getWidth(), y = self.coin:getHeight()}
        )
    end

    function entity:Get_Y_Body(dt)
        return Game.Physics:Create_Rect(
            {x = self.Position.x, y = self.Position.y},
            {x = self.coin:getWidth(), y = self.coin:getHeight()}
        )
    end

    
    local w = math.floor(self.coin:getWidth()*(self.ratioMoneda))
    local h = math.floor(self.coin:getHeight()*(self.ratioMoneda))

    entity.Position = {x = entity.Position.x + (entity.Size.x/2) - w/2, y = entity.Position.y + (entity.Size.y/2) - h/2}
    entity.Size = {x = w, y = h}

    table.insert(self.drops, entity)
end



function Physics_Engine:Tick(dt)


    -- Colisiones del player (bodies)
    for i, body in ipairs(self.bodies) do

        self.target = self:selectClosestNPC(body,dt)

        -- Rect del body actual
        local v_body = self:Create_Rect(body.Position, body.Size)
        -- Rects del body_x y body_y aplicando la velocidad
        local x_body = body:Get_X_Body(dt)
        local y_body = body:Get_Y_Body(dt)
        -- Rects de los walkboxes aplicando la velocidad
        local x_walkbox_body = body:Get_X_BodyWalkbox(dt)
        local y_walkbox_body = body:Get_Y_BodyWalkbox(dt)
        -- Rects de los hitboxes aplicando la velocidad
        local x_hitboxes_body = body:Get_X_BodyHitboxes(dt)
        local y_hitboxes_body = body:Get_Y_BodyHitboxes(dt)

        -- Inmortalidad maximo 1.5 segundos
        if body.Inmortal then
            tiempoInmortal = tiempoInmortal - dt
            if tiempoInmortal <= 0 then
                body.Inmortal = false
                body.parpadeando = false
                tiempoInmortal = 1.5
            end
        end

        if body.atacando then
            body:Decel()
            tiempoAtacando = tiempoAtacando - dt
            if tiempoAtacando <= 0 then
                body.atacando = false
                body.accion = 1
                if body.animActual == "meleeDch" then body.animActual = "derecha"
                elseif body.animActual == "meleeIzq" then body.animActual = "izquierda"
                elseif body.animActual == "meleeArr" then body.animActual = "arriba"
                elseif body.animActual == "meleeAbj" then body.animActual = "abajo"
                end
                e.spriteActual = love.graphics.newQuad(0,0,math.floor(_G[e.type].spriteSheets[1]:getWidth()/_G[e.type].numFrames),
                                                math.floor(_G[e.type].spriteSheets[1]:getHeight()/_G[e.type].numAnimations),
                                                _G[e.type].spriteSheets[1]:getWidth(),
                                                _G[e.type].spriteSheets[1]:getHeight())
                tiempoAtacando = 0.3
            end
        end

        -- Colisiones con el terreno
        for j, static in ipairs(self.statics) do
            local static_body = self:Create_Rect(static.Position, static.Size)
            -- static x_body collision
            if (static_body:IntersectsBody(x_walkbox_body)) then
                x_body.pos.x = v_body.pos.x
            end
            -- static y_body collision
            if (static_body:IntersectsBody(y_walkbox_body)) then
                y_body.pos.y = v_body.pos.y
            end
        end

        -- Colisiones con los drops
        for j, drop in ipairs(self.drops) do
            --print(drop.Position.x, drop.Position.y)
            --love.graphics.draw(self.coin, drop.Position.x, drop.Position.y)
            local drop_body = self:Create_Rect(drop.Position, drop.Size)
            -- static x_body collision
            if (drop_body:IntersectsBody(x_body)) then
                Game.Sounds.coin:stop()
                Game.Sounds.coin:play()
                self:AddMoney(body, drop.value)
                if drop.Remove then
                    table.remove(self.drops,j)
                end
            end
            -- static y_body collision
            if (drop_body:IntersectsBody(y_body)) then
                Game.Sounds.coin:stop()
                Game.Sounds.coin:play()
                self:AddMoney(body, drop.value)
                if drop.Remove then
                    table.remove(self.drops,j)
                end
            end
        end

        -- Colisiones con los cambios de mapa
        for j, map_body in ipairs(self.map_change) do
            local map_change_body = self:Create_Rect(map_body.Position, map_body.Size)
            -- map_change
            if (map_change_body:IntersectsBody(x_walkbox_body)) or (map_change_body:IntersectsBody(y_walkbox_body)) then
                Game.GSM:GotoScene(require (map_body.Map ))
                 Game.Camera:Set()
                 e.camera_smoothing = 0
                 Game.Camera:SetX(map_body.SpawnX)
                 Game.Camera:SetY(map_body.SpawnY)
                 e.camera_smoothing = 0.08
                 Game.Camera:Unset()

                  x_body.pos.x= map_body.SpawnX
                  y_body.pos.y= map_body.SpawnY
            end
        end


        -- Colisiones con los NPC
        for j, npc_body in ipairs(self.bodies_npc) do
            if npc_body.Remove then
                table.remove(self.bodies_npc,j)
            end

            local angle = atan2((body.Position.y+body.Size.y)/2-(npc_body.Position.y+npc_body.Size.y)/2,
                                (body.Position.x+body.Size.x)/2-(npc_body.Position.x+npc_body.Size.x)/2) *180/math.pi

            -- Trackear al jugador
            self:trackPlayer(npc_body, angle)

            -- hitboxes x e y npc
            local npc_bodyhitboxesX = npc_body:Get_X_BodyHitboxes(dt)
            local npc_bodyhitboxesY = npc_body:Get_X_BodyHitboxes(dt)

            for x, npc_hitboxX in ipairs(npc_bodyhitboxesX) do
                for i, x_hitbox in ipairs(x_hitboxes_body) do
                    if npc_hitboxX:IntersectsBody(x_hitbox) then
                        makeDamage(body, npc_body.Damage)
                        knockback(body, angle)
                        br = true
                    end
                    if br then
                        br = false
                        break
                    end
                end
            end

            -- npc y_body collision
            for y, npc_hitboxY in ipairs(npc_bodyhitboxesY) do
                for i, y_hitbox in ipairs(y_hitboxes_body) do
                    if npc_hitboxY:IntersectsBody(y_hitbox) then
                        makeDamage(body, npc_body.Damage)
                        knockback(body, angle)
                        br = true
                    end
                    if br then
                        br = false
                        break
                    end
                end
            end
        end


        body:Decel()
        body.Position.x = x_body.pos.x
        body.Position.y = y_body.pos.y
    end

    local br = false

    -- Colisiones de NPCs (body_npc)
    for i, body_npc in ipairs(self.bodies_npc ) do
        local v_body_npc = self:Create_Rect(body_npc.Position, body_npc.Size)
        local x_body_npc = body_npc:Get_X_Body(dt)
        local y_body_npc = body_npc:Get_Y_Body(dt)
        local x_walkbox_npc = body_npc:Get_X_BodyWalkbox(dt)
        local y_walkbox_npc = body_npc:Get_Y_BodyWalkbox(dt)
        local x_hitboxes_npc = body_npc:Get_X_BodyHitboxes(dt)
        local y_hitboxes_npc = body_npc:Get_Y_BodyHitboxes(dt)
        local x_collisionbox_npc = body_npc:Get_X_BodyCollisionbox(dt)
        local y_collisionbox_npc = body_npc:Get_Y_BodyCollisionbox(dt)

        --player colision
        for _, body in ipairs(self.bodies) do
            local body_hitboxesX = body:Get_X_BodyHitboxes(dt)
            local body_hitboxesY = body:Get_X_BodyHitboxes(dt)

            --local bdy = self:Create_Rect(body.Position, body.Size)
            -- player x_body collision
            for _, body_hitboxX in ipairs(body_hitboxesX) do
                for _, x_hitbox in ipairs(x_hitboxes_npc) do
                    if body_hitboxX:IntersectsBody(x_hitbox) then
                        local angle = math.atan2((body_npc.Position.y+body_npc.Size.y)/2-(body.Position.y+body.Size.y)/2,
                                (body_npc.Position.x+body_npc.Size.x)/2-(body.Position.x+body.Size.x)/2) *180/math.pi
                        makeDamage(body, body_npc.Damage)
                        knockback(body, angle+180)
                        --x_body_npc.pos.x = v_body_npc.pos.x
                        br = true
                    end
                    if br then
                        br = false
                        break
                    end
                end
            end

            -- player y_body collision
            for y, body_hitboxY in ipairs(body_hitboxesY) do
                for i, y_hitbox in ipairs(y_hitboxes_npc) do
                    if body_hitboxY:IntersectsBody(y_hitbox) then
                        local angle = math.atan2((body_npc.Position.y+body_npc.Size.y)/2-(body.Position.y+body.Size.y)/2,
                                (body_npc.Position.x+body_npc.Size.x)/2-(body.Position.x+body.Size.x)/2) *180/math.pi
                        makeDamage(body, body_npc.Damage)
                        knockback(body, angle+180)
                        --y_body_npc.pos.y = v_body_npc.pos.y
                        br = true
                    end
                    if br then
                        br = false
                        break
                    end
                end
            end
        end

        -- Colisiones con los NPC
        --for j, npc_body in ipairs(self.bodies_npc) do
--
        --    if not (j == i) then
        --        -- hitboxes x e y npc
        --        local npc_bodyhitboxesX = npc_body:Get_X_BodyHitboxes(dt)
        --        local npc_bodyhitboxesY = npc_body:Get_X_BodyHitboxes(dt)
--
        --        for _, npc_hitboxX in ipairs(npc_bodyhitboxesX) do
        --            for _, x_hitbox in ipairs(x_hitboxes_npc) do
        --                if x_hitbox:IntersectsBody(npc_hitboxX) then
        --                    x_body_npc.pos.x = v_body_npc.pos.x
        --                    br = true
        --                end
        --                if br then
        --                    br = false
        --                    break
        --                end
        --            end
        --        end
        --        --body_npc.Position.x = x_body_npc.pos.x
--
--
        --        -- npc y_body collision
        --        for _, npc_hitboxY in ipairs(npc_bodyhitboxesY) do
        --            for i, y_hitbox in ipairs(y_hitboxes_npc) do
        --                if y_hitbox:IntersectsBody(npc_hitboxY) then
        --                    y_body_npc.pos.y = v_body_npc.pos.y
        --                    br = true
        --                end
        --                if br then
        --                    br = false
        --                    break
        --                end
        --            end
        --        end
        --        --body_npc.Position.y = y_body_npc.pos.y
        --    end
        --end

        -- Colisiones npc-npc
        
        for j, npc_body in ipairs(self.bodies_npc) do
            if not (i==j) then
                local angle = atan2((npc_body.Position.y+npc_body.Size.y)/2-(body_npc.Position.y+body_npc.Size.y)/2,
                                (npc_body.Position.x+npc_body.Size.x)/2-(body_npc.Position.x+body_npc.Size.x)/2) *180/math.pi
                
                --local npc_bodyRect = self:Create_Rect(npc_body.Position, npc_body.Size)
                local npc_bodyRect = self:Create_Rect( {x = npc_body.Position.x+_G[npc_body.type].Collisionbox[1], y = npc_body.Position.y+_G[npc_body.type].Collisionbox[2]},
                                                       {x = _G[npc_body.type].Collisionbox[3], y = _G[npc_body.type].Collisionbox[4]})
                if (npc_bodyRect:IntersectsBody(x_collisionbox_npc)) then
                    x_body_npc.pos.x = v_body_npc.pos.x
                end

                -- 
                if (npc_bodyRect:IntersectsBody(y_collisionbox_npc)) then
                    y_body_npc.pos.y = v_body_npc.pos.y
                end
            end   
        end


        --static colision
        for j, static in ipairs(self.statics) do
           local static_body = self:Create_Rect(static.Position, static.Size)
            -- static x_body collision
            if (static_body:IntersectsBody(x_walkbox_npc)) then
                x_body_npc.pos.x = v_body_npc.pos.x 
            end
            -- static y_body collision
            if (static_body:IntersectsBody(y_walkbox_npc)) then
                y_body_npc.pos.y = v_body_npc.pos.y
            end
        end

        body_npc:Decel()
        body_npc.Position.x = x_body_npc.pos.x
        body_npc.Position.y = y_body_npc.pos.y
    end 
end


function Physics_Engine:Debug_Render()
    if(Debug_Mode == true) then

        -- Pinta solidos de MORADO
        love.graphics.setColor(255, 0, 255)
        for i, static in ipairs(self.statics) do
            love.graphics.rectangle("line",static.Position.x, static.Position.y, static.Size.x, static.Size.y)
        end

        -- Pinta cambios de mapa de AZUL
        love.graphics.setColor(0, 0, 255)
        for i, map_change in ipairs(self.map_change) do
            love.graphics.rectangle("line",map_change.Position.x, map_change.Position.y, map_change.Size.x, map_change.Size.y)
        end

        -- Pinta cuerpo player de BLANCO
        for i, body in ipairs(self.bodies) do
            love.graphics.setColor(255, 255, 255)
            love.graphics.rectangle("line",body.Position.x, body.Position.y, body.Size.x, body.Size.y)
            -- Pinta hitboxes del player de ROJO
            love.graphics.setColor(255, 0, 0)
            for i, hitbox in ipairs(_G[body.type].Hitboxes) do
                love.graphics.rectangle("line", body.Position.x+hitbox[1], body.Position.y+hitbox[2], hitbox[3], hitbox[4])
            end
            -- Pinta collisionboxes del player de AMARILLO
            love.graphics.setColor(255, 255, 0)
            local collisionbox = _G[body.type].Collisionbox
            love.graphics.rectangle("line", body.Position.x+collisionbox[1], body.Position.y+collisionbox[2], collisionbox[3], collisionbox[4])
            -- Pinta walkbox del player de NEGRO
            love.graphics.setColor(0, 0, 0)
            local walkbox = _G[body.type].Walkbox
            love.graphics.rectangle("line", body.Position.x+walkbox[1], body.Position.y+walkbox[2], walkbox[3], walkbox[4])
        end
        
        -- Pinta cuerpo npc de VERDE
        for i, body_npc in ipairs(self.bodies_npc) do
            love.graphics.setColor(0, 255, 0)
            love.graphics.rectangle("line",body_npc.Position.x, body_npc.Position.y, body_npc.Size.x, body_npc.Size.y)
            -- Pinta hitboxes del npc de ROJO
            love.graphics.setColor(255, 0, 0)
            for i, hitbox in ipairs(_G[body_npc.type].Hitboxes) do
                love.graphics.rectangle("line", body_npc.Position.x+hitbox[1], body_npc.Position.y+hitbox[2], hitbox[3], hitbox[4])
            end
            -- Pinta collisionboxes del npc de AMARILLO
            love.graphics.setColor(255, 255, 0)
            local collisionbox = _G[body_npc.type].Collisionbox
            love.graphics.rectangle("line", body_npc.Position.x+collisionbox[1], body_npc.Position.y+collisionbox[2], collisionbox[3], collisionbox[4])
            -- Pinta walkbox del npc de NEGRO
            love.graphics.setColor(0, 0, 0)
            local walkbox = _G[body_npc.type].Walkbox
            love.graphics.rectangle("line", body_npc.Position.x+walkbox[1], body_npc.Position.y+walkbox[2], walkbox[3], walkbox[4])
        end

        -- Pinta drops de AMARILLO
        love.graphics.setColor(255, 255, 0)
        for i, drop in ipairs(self.drops) do
            love.graphics.rectangle("line",drop.Position.x, drop.Position.y, drop.Size.x, drop.Size.y)
        end

        love.graphics.setColor(255,255,255, 255)
    end
end

function makeDamage(body, dmg)
    if body.Inmortal == false then
        body.Health = body.Health - dmg
        Game.UI:ActualizaHP(body.MaxHealth, body.Health)
        body.parpadeando = true
        body.Inmortal = true
    end
end

function knockback(body, angle)
    if body.Health > 0 then
        local frict = body.Friction
        body.Friction = 0
        body:Decel()
        body.Friction = frict
        body:Apply_Velocity(180, math.rad(angle))
        body:Apply_Velocity(120, math.rad(angle))
        body:Apply_Velocity(100, math.rad(angle))
    end
end

function Physics_Engine:AddMoney(e, money)
    e.Money = e.Money + money
    Game.UI:ActualizaMoney(e.Money)
end

function Physics_Engine:selectClosestNPC(body, dt)
    local dmin = 9999
    local index = 0
    local nul = { type = "nul"}
    for i,npc in ipairs(Physics_Engine.bodies_npc) do
        local dist = distanceFrom(body.Position.x, body.Position.y, npc.Position.x, npc.Position.y)
        if dist < dmin then
            dmin = dist
            index = i
        end
    end

    if index==0 or dmin>Game.Dim.h/2 then
        return nul
    else
        return Physics_Engine.bodies_npc[index]
    end

end

function Physics_Engine:inRange(body, dt)
    local dmin = 9999
    local index = 0
    local nul = { type = "nul"}
    for i,npc in ipairs(Physics_Engine.bodies_npc) do
        local dist = distanceFrom(body.Position.x, body.Position.y, npc.Position.x, npc.Position.y)
        if dist < dmin then
            dmin = dist
            index = i
        end
    end

    if index==0 or dmin>Game.Dim.h/2 then
        return nul
    else
        return Physics_Engine.bodies_npc[index]
    end

end

function distanceFrom(x1,y1,x2,y2) return math.sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2) end

function Physics_Engine:resetTimerAtacando()
    tiempoAtacando = 0.3
end

function Physics_Engine:trackPlayer(npc, angle)
    npc:Apply_Velocity(npc.Speed, math.rad(angle))
end

function Physics_Engine:Render()
    for i, drop in ipairs (self.drops) do
        love.graphics.push()

        love.graphics.scale(self.ratioMoneda,self.ratioMoneda)
        love.graphics.draw(self.coin, drop.Position.x/(self.ratioMoneda), drop.Position.y/(self.ratioMoneda))
		
		--love.graphics.draw(self.sprite, self.posScale.x, self.posScale.y)
		love.graphics.pop()
    end
end

return Physics_Engine