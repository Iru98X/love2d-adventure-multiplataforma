local Sounds = {
    menu = love.audio.newSource("assets/musica/musicaMenu.ogg","static"),
    boton = love.audio.newSource("assets/musica/menuButton.ogg","static"),
    shot = love.audio.newSource("assets/musica/gun_pistol_shot.ogg","static"),
    coin = love.audio.newSource("assets/musica/collect_coin.ogg","static"),
    explosion = love.audio.newSource("assets/musica/explosion.ogg","static"),
    sword = { love.audio.newSource("assets/musica/melee sounds/sword_sound.wav","static"),
              love.audio.newSource("assets/musica/melee sounds/melee_sound.wav","static"),
              love.audio.newSource("assets/musica/melee sounds/sharp_melee_sound.wav","static")}
}

function Sounds:Init()
    self.sword[1]:setPitch(1.7)
    self.sword[2]:setPitch(1.7)
    self.sword[3]:setPitch(1.7)
end
    
function Sounds:playSound(sound)
    sound:stop()
    sound:play()
end

function Sounds:playRandSound(sounds)
    for _, v in ipairs (sounds) do
        v:stop()
    end
    sounds[love.math.random(#sounds)]:play()
end

return Sounds