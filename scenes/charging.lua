
-- Representa un nivel o pantalla del juego
local charging  = require "scenes/scene".New("charging")

local Entity  = require "entities/entity"

local e2 = {}

function charging:OvInit()

	--Para la musica de la escena anterior
    Game.Sounds.menu:stop()
    
    loadChar()
	local pause = require("controles/botones/botonPause").New("assets/ui/pause.jpg",Game.Dim.w-45*Game.Scale,15,Game.Scale/2.3,true,true,"pause")
	local pause = require("controles/botones/botonNuke").New("assets/misc/bomb.png",25*Game.Scale,70*Game.Scale,Game.Scale/9,true,true,"pause")
	
	local attack = require("controles/botones/botonAttack").New("assets/ui/target.png",math.floor((6/7)*Game.Dim.w),math.floor((6/7)*Game.Dim.h),Game.Scale/12,true,true,"attack")
	local hp = require("controles/botonNoClickable").New("assets/ui/hp/hp42.png",math.floor(Game.Dim.w/2),math.floor((18/20)*Game.Dim.h),Game.Scale/3,true,"hp")
	local gold = require("controles/botonNoClickable").New("assets/ui/gold.png",math.floor((18/20)*Game.Dim.w),math.floor((1/5)*Game.Dim.h),Game.Scale/18,true,"gold")
	Game.UI:AddText(e.Money,{x=math.floor((18/20)*Game.Dim.w) + 30*Game.Scale, y = math.floor((1/5)*Game.Dim.h)},"money")
	Game.UI:AddText(e.Health.." / "..e.MaxHealth,{x=math.floor(Game.Dim.w/2)-11*Game.Scale, y = math.floor((18/20)*Game.Dim.h)-2*Game.Scale},"vida")
    Game.GSM:GotoScene(require "scenes/level_0_1")
end


function loadChar()
	e = Game.World:Create_Entity("player")
	e:Add(require "components/c_sprite".New(), {type = "Lpc", velAnimacion = 0.3})
	e:Add(require "components/c_body".New(), {x = 400, y = 600})
	e:Add(require "components/c_stats".New(), {speed = 18, maxHealth = 60, damage = 25})
	e:Add(require "components/c_inventory".New(), {})
	e:Add(require "components/c_physics".New(), {friction = .83})
	e:Add(require "components/c_controles".New(), {})
	e:Add(require "components/c_camera_follow".New(), {})
end

function charging:OvTick()

end

function charging:OvRender()

end


return charging