

-- Representa un nivel o pantalla del juego
local ciudad_0_3 = require "scenes/scene".New("ciudad_0_3")

local Entity  = require "entities/entity"

function ciudad_0_3:OvInit()

    Game.Physics:Reset()
    
    -- cambio mapa
    Game.Assets:Add(love.graphics.newImage("assets/maps/ciudad.png"), "ciudad")
    Game.Assets:Generate_Quads(64, Game.Assets:Get("ciudad"), "ciudad_quads")

    -- Mapa

    Game.mapActual = require("scenes/tile_map").New("assets/maps/ciudad")
end

function ciudad_0_3:OvRender()

end

return ciudad_0_3