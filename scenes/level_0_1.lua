
-- Representa un nivel o pantalla del juego
local level_0_1  = require "scenes/scene".New("level_0_1")

local Entity  = require "entities/entity"

local e2 = {}

function level_0_1:OvInit()
	
	-- Reset de los objetos que hay en las fisicas
	Game.Physics:Reset()
	
    -- Objetos solidos
    e2 = Game.World:Create_Entity("skele")
    e2:Add(require "components/c_sprite".New(), {type = "Skeleton", velAnimacion = 0.3})
    e2:Add(require "components/c_body".New(), {x = 500, y = 180})
	e2:Add(require "components/c_stats".New(), {health = 100, damage = 20, speed = 8})
	e2:Add(require "components/c_physics_npc".New(), {friction = .85})

	e3 = Game.World:Create_Entity("knight")
    e3:Add(require "components/c_sprite".New(), {type = "Knight", velAnimacion = 0.3})
    e3:Add(require "components/c_body".New(), {x = 100, y = 500})
	e3:Add(require "components/c_stats".New(), {health = 200, damage = 20, speed = 10})
	e3:Add(require "components/c_physics_npc".New(), {friction = .85})

    -- Mapa
	Game.mapActual = require("scenes/tile_map").New("assets/maps/mapa_0_1")
end

spawn = 0
vueltuca = 0
function level_0_1:OvTick()
	spawn = spawn + 1

	if spawn%600 == 0 then
		local randomNumber = love.math.random(1, 3)
		local randomSpawn = {x = love.math.random(1, 500), y = love.math.random(1, 500)}
		for i=1,randomNumber do
			var = love.math.random(1, 500)
			_G['var'] = Game.World:Create_Entity("skele")
			_G['var']:Add(require "components/c_sprite".New(), {type = "Skeleton", velAnimacion = 0.3})
			_G['var']:Add(require "components/c_body".New(), {x = love.math.random(1, 500), y = love.math.random(1, 500)})
			_G['var']:Add(require "components/c_physics_npc".New(), {friction = .85})
			_G['var']:Add(require "components/c_stats".New(), {health = 100, damage = 20, speed = 8})
			local var = love.math.random(1, 500)
			_G['var'] = Game.World:Create_Entity("knight")
			_G['var']:Add(require "components/c_sprite".New(), {type = "Knight", velAnimacion = 0.3})
			_G['var']:Add(require "components/c_body".New(), {x = love.math.random(1, 500), y = love.math.random(1, 500)})
			_G['var']:Add(require "components/c_physics_npc".New(), {friction = .85})
			_G['var']:Add(require "components/c_stats".New(), {health = 200, damage = 20, speed = 10})
		end
		spawn = 0
	end

	local vueltas = false
	if vueltas then
		-- me aburro xD
		--print(vueltuca)
		if vueltuca >= 0 and vueltuca < 200 then
			e2:Apply_Velocity(5, math.rad(180))
			e3:Apply_Velocity(8, math.rad(315))
			vueltuca = vueltuca + 1
		end 

		if vueltuca >= 200 and vueltuca < 400 then
			e2:Apply_Velocity(5, math.rad(270))
			e3:Apply_Velocity(8, math.rad(45))
			vueltuca = vueltuca + 1
		end 

		if vueltuca >= 400 and vueltuca < 600 then
			e2:Apply_Velocity(5, math.rad(0))
			e3:Apply_Velocity(8, math.rad(135))
			vueltuca = vueltuca + 1
		end 

		if vueltuca >= 600 and vueltuca < 800 then
			e2:Apply_Velocity(5, math.rad(90))
			e3:Apply_Velocity(8, math.rad(225))
			vueltuca = vueltuca + 1
		end 

		if vueltuca >= 800 then
		vueltuca = 0
		end 
	end

end

function level_0_1:OvRender()

end

return level_0_1