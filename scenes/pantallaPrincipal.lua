-- Representa un nivel o pantalla del juego
local pantallaPrincipal  = require "scenes/scene".New("pantallaPrincipal")


function pantallaPrincipal:OvInit()
    bg = love.graphics.newImage("assets/fondos/menu.jpg")
    Game.Sounds.menu:setLooping(true)
    Game.Sounds.menu:play()
    sx = Game.Dim.w / bg:getWidth()
    sy = Game.Dim.h / bg:getHeight()
    local debug = require("controles/botones/botonDebug").New("assets/controles/botones/depuracion.png",15,15,Game.Scale,true,true,"depuracion")
    local start = require("controles/botones/botonPlay").New("assets/controles/botones/start.png",Game.Dim.w/2,Game.Dim.h/2-30,Game.Scale/2,true,true,"play")
end

function pantallaPrincipal:OvTick()

end

function pantallaPrincipal:OvRender()
    love.graphics.draw(bg, bg.x, bg.y, 0, math.max(sx,sy))
end

return pantallaPrincipal