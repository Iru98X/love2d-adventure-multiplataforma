require "bit"

local Tile_Map = {

    New = function(path)

        -- Bits on the far end of the 32-bit global tile ID are used for tile flags (flip, rotate)
        --local FLIPPED_HORIZONTALLY_FLAG = 0x80000000;
        --local FLIPPED_VERTICALLY_FLAG = 0x40000000;
        --local FLIPPED_DIAGONALLY_FLAG = 0x20000000;
    
        local map = require(path)

        local layers = map.layers
        local tileset = map.tilesets[1]
        map.tileset=tileset

        map.animatedTiles = {}
        for i, tile in ipairs(tileset.tiles) do
            map.animatedTiles[tile.id] = tile
        end
        
        map.frame = 0
        map.timer = 0.0
        map.maxTimer = 0.1
        


        for i, layer in ipairs(layers) do
            if (layer.type == "objectgroup") then
                for j, object in ipairs(layer.objects) do


                    if (object.type == "") then
                        Game.Physics:Add_Static_Body({
                            position = {x=object.x, y=object.y},
                            size = {x=object.width, y=object.height}
                        })
                    end

                    if (object.type == "static") then
                        Game.Physics:Add_Static_Body({
                            position = {x=object.x, y=object.y},
                            size = {x=object.width, y=object.height}
                        })
                    end

                    if (object.type == "teleport") then

                        Game.Physics:Add_Map_Change({
                            position = {x=object.x, y=object.y},
                            size = {x=object.width, y=object.height},
                            map= object.properties.map,
                            spawnX= object.properties.spawnX,
                            spawnY= object.properties.spawnY
                        })
                    end
                    
                end
            end
        end

        function map:Tick(dt)
            --print(map)
            if self.timer > self.maxTimer then 
                self.frame = self.frame + 1
                self.timer = 0
            end
            self.timer = self.timer + dt
        end

        function map:Render()
            local image = Game.Assets:Get(map.tilesets[1].name)
            assert(image)

            local quads = Game.Assets:Get(map.tilesets[1].name .. "_quads")
            assert(quads)



            for i, layer in ipairs(self.layers) do
                if (layer.type == "tilelayer") then
                    for y = 0, layer.height - 1 do
                        for x = 0, layer.width - 1 do
                            local id = layer.data[(x + y * layer.width) + 1]
                            if (id ~= 0) then

                                --local flipHor
                                --local flipVer
                                --local flipDia

                                -- Read flipping flags
                                --flipHor = bit.band(id, FLIPPED_HORIZONTALLY_FLAG)
                                --flipVer = bit.band(id, FLIPPED_VERTICALLY_FLAG)
                                --flipDia = bit.band(id, FLIPPED_DIAGONALLY_FLAG)

                                -- Convert flags to bit equiv
                                --if(flipHor ~= 0) then flipHor = 4 end --TileMap.FLIP_HORIZONTAL end
                                --if(flipVer ~= 0) then flipVer = 2 end --TileMap.FLIP_VERTICAL end
                                --if(flipDia ~= 0) then flipDia = 1 end --TileMap.FLIP_DIAGONAL end

                                -- Clear the flags from id so other information is healthy
                                --id = bit.band(id, bit.bnot(bit.bor(FLIPPED_HORIZONTALLY_FLAG, FLIPPED_VERTICALLY_FLAG, FLIPPED_DIAGONALLY_FLAG)))

                                if self.animatedTiles[id - 1] ~= nil then
                            
                                    local anim = self.animatedTiles[id - 1].animation
                                    local numFrames = #anim
                                    local index = self.frame % numFrames

                                    id = anim[index + 1].tileid + 1
                                end
                                
                                --if(flipHor ~= 0) then flipHor = -1 else flipHor = 1 end
                                --if(flipVer ~= 0) then flipVer = -1 else flipVer = 1 end
                                --print(flipHor,flipVer)

                                --if flipDia
                                love.graphics.draw(image, quads[id], x * self.tilewidth, y * self.tileheight,0,1,1)
                            end
                        end
                    end
                end
            end
        end



        function map:Destroy()
            --print(tostring(self).." Destruido")
            Game.GameLoop:RemoveLoop(self)
            Game.Renderer:RemoveRendererLayer(2)
            --print("LOOP Y CAPA DE RENDER DE MAPAS DESTRUIDAS!")
        end

        Game.GameLoop:AddLoop(map)
        Game.Renderer:AddRenderer(map, 2)
        --print(tostring(map).." MAPA ANADIDO!")
        return map
    end,
}
return Tile_Map