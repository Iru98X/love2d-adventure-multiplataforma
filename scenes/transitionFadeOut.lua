
-- Transicion de difuminado a negro
local transitionFadeOut = require "scenes/scene".New("transitionFadeOut")

local a = 0 -- ratio de transparencia inicial
local seconds = 1 -- segundos de media transicion
local seconds_negro = 1 -- segundos en negro
local timerNegro = seconds_negro
local ida = true

function transitionFadeOut:Init()

    Game.Physics:Reset()

    -- Mapa
    --Game.mapActual = require("scenes/tile_map").New("assets/maps/mapa_0_2")
end

function transitionFadeOut:Tick(dt)
    -- Transicion a negro
    if ida then
        a = math.min(a + (255/seconds*dt), 255)
        if a == 255 then
            ida = false
        end
    end
end

function transitionFadeOut:Render()
    love.graphics.setColor(0,0,0,a)
    love.graphics.draw(Game.Fondo, 0, 0)
    love.graphics.setColor(255,255,255,255)
end

return transitionFadeOut

-- Pantalla negra
-- if timerNegro > 0 then
--     timerNegro = math.max(0, timerNegro - (1/seconds_negro*dt))
-- -- Transicion a transparente
-- else
--     a = math.max(0, a - (255/seconds*dt))
-- end